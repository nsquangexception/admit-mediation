//
//  RakutenRewardCustomEventInterstitial.swift
//  SamplePhotosApp iOS
//
//  Created by Ha, Quang on 2018/09/20.
//  Copyright © 2018 Apple. All rights reserved.
//

import Foundation
import GoogleMobileAds

@objc class RakutenRewardCustomEventInterstitial : NSObject {
    var admobDelegate: GADCustomEventInterstitialDelegate?
}

extension RakutenRewardCustomEventInterstitial : GADCustomEventInterstitial {
    var delegate: GADCustomEventInterstitialDelegate? {
        get {
            return admobDelegate
        }
        set(delegate) {
            admobDelegate = delegate
        }
    }
    
    
    func requestAd(withParameter serverParameter: String?, label serverLabel: String?, request: GADCustomEventRequest) {
        
    }
    
    func present(fromRootViewController rootViewController: UIViewController) {
        
    }
    
    
}
